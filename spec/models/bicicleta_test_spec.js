var mongoose = require('mongoose');
var Bicicleta = require("../../models/bicicleta");

describe('Testing Bicicletas',function(){
    beforeEach(function(done){
        
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB,
            {
                useNewUrlParser : true,
                useUnifiedTopology: true
            });
        
        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:')); 
        db.once('open', function() {
            console.log('Conectado a la base de datos');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({},function(err,success){
            if(err)
                console.log(err);
            done();
        });
    });


    describe('Bicicleta.createInstance',() => {
        it('Crea una instancia de la bicicleta',() => {
            var prueba = {code : 1, color : 'verde' , modelo : 'urbana' , ubicacion : [6.2443382, -75.573553]};
            var bici = Bicicleta.createInstance(prueba.code , prueba.color , prueba.modelo , prueba.ubicacion);
            expect(bici.code).toBe(prueba.code);
        });
    });


    describe('Bicicleta.allBicis',() => {
        it('Comienza vacia',(done) => {
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                done();
            })
        })
    });


    describe('Bicicleta.add',() => {
        it('Agrega solo una bicicleta',(done) => {
            var aBici = new Bicicleta({code : 1, color: 'verde', modelo : 'urbana', ubicacion :[-34.878989,-54.989880]});
            Bicicleta.add(aBici,function(err,newBici){
                //console.info('--add ',newBici);
                if(err)
                    console.log(err);
                Bicicleta.allBicis(function(err,bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        })
    });


    describe('Bicicleta.findByCode',() => {
        var codePrue = 1;
        var codePrue2 = 2;
        it(`Debe ingresar y recuperar bici con el código ${codePrue}`,(done) => {
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
            });
            
            var aBici = new Bicicleta({code : codePrue, color: 'verde', modelo : 'urbana'});
            Bicicleta.add(aBici,function(err,newBici){
                if(err)
                    console.log(err);
                
                var aBici2 = new Bicicleta({code : codePrue2, color: 'azul', modelo : 'todo terreno'});
                Bicicleta.add(aBici2,function(err,newBici){
                    if(err)
                        console.log(err);
                    Bicicleta.findByCode(codePrue,function(err,targetBici){
                        if(err)
                            console.log(err);
                        expect(targetBici.code).toBe(aBici.code);
                        expect(targetBici.color).toBe(aBici.color);
                        expect(targetBici.modelo).toBe(aBici.modelo);
                        done();
                    })
                });
            });
        })
    })


});

// beforeEach( () => {
//     Bicicleta.allBicis = [];
// });

// describe('Bicicleta.allBicis', () => {

//     it('Comienza vacia', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });


// describe('Bicicleta.add', () => {

//     it('Agregamos Bicicleta', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var aBici = new Bicicleta({code : 1, color: 'verde', modelo : 'urbana', ubicacion :[-34.878989,-54.989880]});

//         Bicicleta.add(aBici);
    
//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(aBici);

//     });
// });


// describe('Bicicleta.findById',() => {

//     it('Debe devolver la Bicicleta con  Id 1', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var aBici = new Bicicleta(1, "Verde", "Urbana");
//         var aBici2 = new Bicicleta(2, "Rojo", "Montaña");

//         Bicicleta.add(aBici);
//         Bicicleta.add(aBici2);

//         var targetBici = Bicicleta.findById(1);
//         expect(targetBici.id).toBe(1);
//         expect(targetBici.color).toBe(aBici.color);
//         expect(targetBici.modelo).toBe(aBici.modelo);

//     })

// })