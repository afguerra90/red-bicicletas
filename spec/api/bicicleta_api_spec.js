var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

const uriserver='http://localhost';
const serverPort = ':3000';
const OK = 200;
const CLEAR = 204;
const uri_get_bicicletas = uriserver+serverPort+'/api/bicicletas';
const uri_post_bicicletas = uriserver+serverPort+'/api/bicicletas/create';
const uri_delete_bicicletas = uriserver+serverPort+'/api/bicicletas/delete';
const uri_update_bicicletas = uriserver+serverPort+'/api/bicicletas/:id/update';

describe('Probando el API de bicicletas',()=>{
    beforeEach(function(done){
        
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB,
            {
                useNewUrlParser : true,
                useUnifiedTopology: true
            });
        
        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:')); 
        db.once('open', function() {
            console.log('Conectado a la base de datos de test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({},function(err,success){
            if(err)
                console.log(err);
            done();
        });
    });

    describe('GET bicicletas /',()=>{
        it(`Status ${OK}`,(done) => {
            request.get(uri_get_bicicletas,(err,response,body) => {
                if(err)
                    console.log(err);
                console.info('--url',uri_get_bicicletas);
                console.info('--body',body);
                console.info('--status',response.statusCode);
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(OK);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        })
    });

    
    describe('POST bicicletas /create',()=>{
        it(`Status ${OK}`,(done)=> {

            var headers = {'content-type':'application/json'};
            var aBici = {code:1010,color:'black',modelo:'urbana',lat:-38.676767,lng:-54.989898};

            request.post({
                headers : headers,
                url: uri_post_bicicletas,
                body :  JSON.stringify(aBici)  
            },
            (error,response,body) => {
                if(error)
                    console.info('--error post create bicicletas',error);
                expect(response.statusCode).toBe(OK);
                console.info('--body',body);
                var result = JSON.parse(body).bicicleta;
                console.log(result);
                expect(result.code).toBe(aBici.code);
                expect(result.color).toBe(aBici.color);
                expect(result.modelo).toBe(aBici.modelo);
                expect(result.ubicacion[0]).toBe(aBici.lat);
                expect(result.ubicacion[1]).toBe(aBici.lng);
                done();
            });
        })
    });
    
});



// describe('Bicicleta API', () =>{

//     describe('GET Bicicletas /', () =>{
//         it('Status 200', () =>{
//             expect(Bicicleta.allBicis.length).toBe(0);

//             var a = new Bicicleta(1, 'beige',  'urbana', [-34.878989,-54.989880]);

//             Bicicleta.add(a);

//             request.get('http://localhost:3000/api/bicicletas', (error, response, body) => {
//                 expect(response.statusCode).toBe(200);
//             });
//         });
//     });
    
//     describe('POST Bicicletas /create', () =>{
//         it('Status 200', (done) => {
//             var headers = {'content-type' : 'application/json'};
//             var aBici ={"id": 10, "color": "rojo", "modelo": "urbana", "lat": -34,"lng": -54};

//             request.post({
//                 headers: headers,
//                 url: 'http://localhost:3000/api/bicicletas/create',
//                 body: aBici
//             }, function(error, response, body) {
//                 expect(response.statusCode).toBe(200);
//                 expect(Bicicleta.findById(10).color).toBe("rojo");

//                 done();
//             });
//         });
//     });

// });
